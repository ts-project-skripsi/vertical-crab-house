@extends('admin.layouts.app')

@section('title')
    Jasa Pengiriman
@endsection

@section('content')
  <div class="">
    <div class="d-flex justify-content-between align-items-center">
        <h2 class="text-lg font-medium truncate">Ubah Data Jasa Pengiriman</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('jasa-pengiriman.index')}}" class="btn btn-secondary"> Kembali </a>
        </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
        <form action="{{ route('jasa-pengiriman.update',$jasaPengiriman->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="col-lg-12">
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <label class="form-label">Nama Jasa</label>
                            <input type="text" name="nama" class="form-control form-control-sm" placeholder="Masukkan nama jasa pengiriman" value="{{ $jasaPengiriman->nama }}" required >
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <label class="form-label">Harga</label>
                            <input type="text" name="harga" class="form-control form-control-sm" placeholder="Masukkan harga jasa pengiriman" value="{{ number_format($jasaPengiriman->harga,0,',','.') }}" required data-format_rupiah="formatRupiah" >
                        </div>
                    </div>
                    <div class="col-lg-12 d-flex justify-content-end" >
                        <button type="submit" class="btn btn-success"> Simpan </button>
                    </div>
            </div>
        </form>
    </div>
  </div>
@endsection
